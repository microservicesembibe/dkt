# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Deep Knowledge Tracing
* https://stanford.edu/~cpiech/bio/papers/deepKnowledgeTracing.pdf


### Files  ###
Train_DKT_KG-embedding ->. Notebook for training DKT with KG infusion <br>
Train_DKT_KG_Pre-req ->. Notebook for training DKT with KG infusion & prerequsite loss <br>

Train_GRU_simple -> Notebook for DKT with using GRU <br>
Train_DKT_temporal_feature -> Notebook for DKT with difficulty level & lag time <br>
